function [est model] = baydiff(data,varargin),

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % estimates microstructural diffusion parameters by Bayesian estimation 
    % written by Marco Reisert, Medical Physics, University Hospital Freiburg
    %
    % [est model] = baydiff(data,'parameter1', value1, .....)
    %
    % input   data - a struct containing the signal in data.dwi as a
    %                 [w h d N] array for N diffusion weightings. 
    %                 and data.tensor containing the q-space scheme in 
    %                 a [3 3 N] array (units in s/mm^2)
    %                 loaded with bd_loadData_nii or bd_loadData_mat
    %
    %         parameters - (parameter = default)
    %                 noise = 'estglobal'           either 1) noise level in signal units, 
    %                                               or 2) 'estglobal' global noise level is estimated from b0s
    %                                               or 3) 'estlocal' local noise level is estimated   
    %                                               or 4) a custom SNR map                                              
    %                 sigma_snrestimation = 1.75    smoothing level of SNR map                  
    %                 force_retraining = false      force retraining even if model exists    
    %                 N = 1;                        number of coils (N=1 Rician, N>1 non-central chi with 2N dof)                     
    %                 nt = 1*10^4                   number of training samples                   
    %                 ncross = [1 2 30]             simulated crossing configurations    
    %                 lmax = 2                      SH expansion degree                              
    %                 reg = 0;                      Tikhonov regularizer for Bayes model             
    %                 includeb0 = true              sensitivity to b0                                
    %                 bayesmodel = 'poly'           bayes model type (fourier or poly)               
    %                 order = 3                     complexity of the model (W)                          
    %                 qspace = 'qball'              radial q-sampling ('multishell' or 'qball') 
    %                 verbose = true                show correlations during learing                 
    %
    %  output  est -  contains a [w h d K] array with estimated maps 
    %          model- a struct containing model information including parameter names etc .. 
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    noise = 'estglobal'; % noise level in signal units, if empty snr is estimated from b0s        (badifparam)
    SNRthreshold = 5;
    sigma_snrestimation = 1.75;  % smoothing level of SNR map                            (badifparam)
    sigma_smooth = 0; % smoothing of features                                            (badifparam)
  
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;


    % load or train model
    model = loadModel(data,varargin{:});
   
    % prepare data and SNR map
    ten = data.tensor;
    b = squeeze(ten(1,1,:)+ten(2,2,:)+ten(3,3,:));
    sz = size(data.dwi); 
    if sigma_smooth == 0,
        preproc = @(x) x; 
    else
        gau = makeGau(sigma_smooth,sz);
        sm2 = @(x) real(ifftn(fftn(x).*gau));   
        col = @(x) x(:);
        preproc = @(x) col(sm2(reshape(x,sz(1:3))));
    end;
    b0idx = round(b/100)==0;
    if sum(b0idx) < 6 && not(isnumeric(noise)),
        warning('non enough b=0 images for noise estimation, assuming noise=10')
        noise = 10;
    end;
    data.dwi = single(data.dwi);    
    data.dwi(isnan(data.dwi(:))) = 0;
    b0 = mean(data.dwi(:,:,:,b0idx),4);
    if isstr(noise),

        gau = makeGau(sigma_snrestimation,sz);
        sm = @(x) real(ifftn(fftn(x).*gau));       
        
        if strcmp(noise,'estlocal'),
            err = std(data.dwi(:,:,:,b0idx),[],4);
            SNR = b0./ (eps+err);
        elseif strcmp(noise,'estglobal'),
            mb0 = mean(data.dwi(:,:,:,b0idx),4);
            [~, idx] = sort(mb0(:));
            idx = idx(round(length(idx)*0.6):end);
            err = std(data.dwi(:,:,:,b0idx),[],4);
            err = mean(err(idx));
            SNR = b0./ (eps+err);
            %err
        end;

        SNR = sm(SNR) ;
    else
        if length(size(noise)) == 3,
            noise(isnan(noise)) = 0;
            noise(isinf(noise)) = 0;
            SNR = b0 ./ (eps+noise);
        else
            SNR = b0 / noise;
        end;
    end;       
    
    SNR(SNR>100) = 100;



    S = reshape(data.dwi,[prod(sz(1:3)) sz(4)]);
    S = S./(eps+repmat(b0(:),[1 sz(4)]));  

    display('estimating Diffusion parameters');    
    est = model.apply(SNR(:),S,data.tensor,preproc);
       
    est = reshape(est,[sz(1:3) size(est,2)]);
    est(:,:,:,end+1) = SNR;
    
    
    %%
    mask = b0>0;
    for k = 1:min(11,size(est,4)),
        tmp = est(:,:,:,k);
        tmp(not(mask(:))) = NaN;
        tmp(tmp<0) = 0;
        est(:,:,:,k) = tmp;
    end;
    %%
    
   

function gau = makeGau(sigma,sz);

try
    ng = ceil(sigma*5); ng = ng + mod(ng,2) + 1;
    gau = fspecial('gaussian',[ng 1],sigma); [gx gy gz] = ndgrid(gau); gau = gx.*gy.*gz;
    sz=sz(1:3);
    gau = padarray(gau,floor((sz-ng)/2),'post');
    gau = padarray(gau,floor((sz-ng)/2)-mod(sz,2) +1,'pre');
    gau = fftn(ifftshift(gau));
    gau = gau/gau(1);
catch err
    ng = ceil(sigma*3); ng = ng + mod(ng,2) + 1;
    gau = fspecial('gaussian',[ng 1],sigma); [gx gy gz] = ndgrid(gau); gau = gx.*gy.*gz;
    sz=sz(1:3);
    gau = padarray(gau,floor((sz-ng)/2),'post');
    gau = padarray(gau,floor((sz-ng)/2)-mod(sz,2) +1,'pre');
    gau = fftn(ifftshift(gau));
    gau = gau/gau(1);
    
end;
   
   
   
   
   
   
function model = loadModel(data,varargin)

    force_retraining = false;      % force retraining even if model exists     (badifparam)

    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;


   if isempty(dir(fullfile(tempdir,'baydif')))
          mkdir(fullfile(tempdir,'baydif'));
   end;       
   
   thishashnum = bd_createDWIHashnum(data.tensor);
   thedir = fullfile(tempdir,'baydif',thishashnum);              
   try 
        model = load(fullfile(thedir,'baydif_model.mat'));
        display('trained Bayes model found');
   catch
        model = [];
   end       
   
   if isempty(model) | force_retraining,
       display('training Bayes model');
       model = bd_trainmodel(data,varargin{:});
       if isempty(dir(thedir)),
            mkdir(thedir);
       end;
       save(fullfile(thedir,'baydif_model.mat'),'-struct','model');     
   end;       
    

               