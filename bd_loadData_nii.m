%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%   data = loadData_nii(dwifile,gradfile)
%
%   loads dMRI data from 3D or 4D-nifti, if no parameters are 
%   provided a GUI-dialog pops up asking for the data and the bvec/bval
%   files (FSL-style)
%
%   dwifile - either a cellarray of 3D niftis or a 4D nifti
%   gradfile - a cellarray of the bvec and bval file (FSL-style)
%
%   data - a data struct used for baydiff and stackview_micro
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function data = loadData_nii(dwifile,gradfile)
    
       data = [];

       if not(exist('dwifile')),
            [files path] = uigetfile({'*.nii;*.nii.gz;*.hdr', 'All accepted filetypes'},'Select nii containing DWI information','MultiSelect', 'on');      
            if not(iscell(files)),
                if files==0,
                    return;
                end;
            end;
            if iscell(files),
                dwifile = cellfun(@(x) fullfile(path,x),files,'uniformoutput',false);
            else              
                dwifile = fullfile(path,files);
            end;
            
            [gradf pathgradf] = uigetfile({'*', 'All accepted filetypes'},'Select gradient information files','MultiSelect', 'on'); 
            
            
            if length(gradf)~=2,
                [~,~,ext] = fileparts(gradf);
                if strcmp(ext,'.bmat')
                    gradfile{1} = fullfile(pathgradf,gradf);
                else                                
                    errordlg('You have to select 2 files containing gradient directions and b-values (FSL style)');
                    return;
                end;
            else 
                gradfile{1} = fullfile(pathgradf,gradf{1});
                gradfile{2} = fullfile(pathgradf,gradf{2});
            end;
            
         
       end;
                
       if iscell(dwifile),            
            dwinii = load_untouch_nii(dwifile{1});
            dwinii.img = zeros([size(dwinii.img) length(dwifile)]);
            for k = 1:length(dwifile),
                tmp = load_untouch_nii(dwifile{k});
                dwinii.img(:,:,:,k) = tmp.img;
            end;                
       else
            dwinii = load_untouch_nii(dwifile);
       end;
       
       if length(gradfile) == 2,           
           gradf1 = importdata(gradfile{1});
           gradf2 = importdata(gradfile{2});
           if size(gradf1,1) == 3,
               bvec = gradf1;
               bval = gradf2;
           elseif size(gradf1,2) == 3,
               bvec = gradf1';
               bval = gradf2;
           elseif size(gradf2,1) == 3,
               bvec = gradf2;
               bval = gradf1;
           elseif size(gradf2,2) == 3,
               bvec = gradf2';
               bval = gradf1;
           else
               bvec = gradf2;
               bval = gradf1;
           end;

           bval = bval(:)';

           if size(bvec,1) ~= 3,
               bvec = bvec';
           end;

           for k = 1:size(bval,2),
               gdir = bvec(:,k);
               gdir = gdir / (eps+norm(gdir));
               tensor(:,:,k) = gdir*gdir' *bval(k);
           end;
       else
           tdata = importdata(gradfile{1});
           tensor = reshape(tdata',[3 3 size(tdata,1)]);
           
       end
    
       
       data.dwifile = dwifile;
       data.dwi = dwinii.img;
       data.gradfile = gradfile;
       data.tensor = tensor;
       data.nii = rmfield(dwinii,'img');      
       data.vox = dwinii.hdr.dime.pixdim(2:4);
       data.name = dwinii.fileprefix;
       
       

