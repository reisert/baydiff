function R = chiLogLik(S,M,n,sig)
% S - model signal
% M - measurement
% n - number of coils * 2
% sig - noise level

S = double(S/sig);
M = double(M/sig);
lb = S.*M;
cut = 500;
lb(lb<cut) = log(besseli(n/2-1,lb(lb<cut)));
R = sum ((S.^2 + M.^2)/2 - n/2*log(M) - (1-n/2)*log(S) -lb);

R = R/size(S,1);

R = R/sqrt(2); % approx. of LLik normalization (valid for SNR 5-20)