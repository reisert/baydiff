%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%  res = fitDispersion(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,snr,noisedeg, proto,Smeas,dirs256,disptypes,fod)
%
%  fits dispersion depending on ratios of SH coeffs
%      Dpara_in,Dpara_ex,Dorth,vf,vf_csf  - parameters of the micromodel
%      snr,noisedeg                       - parameters of noise distrib 
%      proto                              - diffusion scheme
%      Smeas                              - measurement 
%      dirs256                            - some dirs 
%      disptypes                          - type of disperision (see dispstick)
%      fod                                - optionally fod
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function res = fitDispersion(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,p2,snr,noisedeg, proto,Smeas,dirs256,disptypes,fod)


    b = proto.b;
    buni = proto.buni;
    dirs = proto.dirs;
   
    ten = proto.ten;
   
 
    n= [];
    weights = [];
    

    usecsd =true;



   %% rebuild signal without dispersion

     lag = @(x) exp(x/2).*((1-x).*besseli(0,-x/2) - x.* besseli(1,-x/2));
     riceexp = @(x,sigma) sigma*sqrt(pi/2) *lag(-x.^2./(2*sigma^2));        
     
     highb = proto.type=='l' & b > 1.5;
     
     if sum(highb) == 0
        highb = b > 0.7;
     end
     
     if sum(highb) == 0
        highb = b > 0;
     end
     
     % do csd 
     if usecsd,
         lmax = 8;
         S = simSig(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,proto,[0 0 1]',1,'nodisp');      

         fod =  csd(Smeas(highb),dirs(:,highb),dirs256, S(highb),lmax);
     else
         fod = [];
     end
     
     % find main direction
     dd = dirs(:,highb);
     T = [dd(1,:).^2 ; dd(2,:).^2 ; dd(3,:).^2 ; 2*dd(1,:).*dd(2,:) ; 2*dd(1,:).*dd(3,:) ; 2*dd(2,:).*dd(3,:) ;dd(1,:)*0+1 ];
     alpha = pinv(T')*Smeas(highb);
     D = [alpha(1) alpha(4) alpha(5) ;...
         alpha(4) alpha(2) alpha(6) ;...
         alpha(5) alpha(6) alpha(3) ];
     if any(isnan(D(:))) || any(isinf(D(:))),
         D = randn(3,3);
     end;
     [U,d] = eigs(D);
     [~,idx] = min(diag(d));
     pd = U(:,idx);

     weights = 1;
     n = pd;

         
             
  %% estimate dispersion based on l=2 sh coefficients            

    S = simSig(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,proto,n,weights,'nodisp');      

    if 1
        kappa = 1;
        [proj_tmp idx_sh] = SH(dirs(:,:),2);        
        for k = 2:length(buni),
            bval = buni(k);
            idx = not((b==bval & proto.type=='l'));
            tmp = proj_tmp;
            tmp(:,idx) = 0; tmp(1,:) = 0;
            pM = S'*tmp';
            pp = Smeas'*tmp';
            pMP(k-1,:) = real(pp*pM');
            pPred2(k-1,:) = sum(abs(pM).^2);
        end;


        aa = pMP./pPred2;
        kappa = aa(2);
        kappa(kappa>1) = 1;
        kappa(kappa<0) = 0;

        p2 = kappa;
    end
    S_nodisp = S;        

    %% rebuild signal with dispersion


    clear S
    for k = 1:length(disptypes);
        if strcmp(disptypes{k},'fod')
            S{k} = simSig(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,proto,n,weights,disptypes{k},fod);  
        else
            S{k} = simSig(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,proto,n,weights,disptypes{k},p2);  
        end
        S00 = double(Smeas)'*double(S{k}) / norm(double(S{k}))^2;
%        fprintf('%s: %f \n',disptypes{k},S00);
        S0(k) = S00;
        S{k} = S{k} * S00;
        errRic(k) = bd_chiLogLik(double(Smeas), double(S{k}),noisedeg*2,double( 1/snr(1)));
%        errnormRic(k) = normRicLogLik(double(Smeas), double(S{k}),noisedeg*2,double( 1/snr(1)));
        errGS(k) = sum((Smeas-S{k}).^2)*snr(1)^2 /length(Smeas);
    end;

    res.S_nodisp = S_nodisp;
    res.S = S;
    res.S0 = S0;
    res.errRic = errRic;
    res.errGS = errGS;
%       res.errnormRic= errnormRic;
    res.fod = fod;
    res.weights = weights;
    res.n = n;
    res.pd = pd;
    res.p2 = p2;
    res.kappa = p2;

 
 
 
      
%% SH transform
function [M idx] = SH(n,lmax)
n = n ./ repmat(sqrt(sum(n.^2)),[3 1]);

M = [];
for l = 0:2:lmax,
    m = legendre(l,n(3,:),'sch');
    n2 = n(1,:)+i*n(2,:)+eps;
    n2 = n2 ./ abs(n2);
    m = m.* (repmat(n2,[size(m,1) 1]).^repmat((0:l)',[1 size(n2,2)]))*sqrt(2*l+1);
    idx1 = size(M,1);
    M = [M ; m(1,:) ; real(m(2:end,:)) ; imag(m(2:end,:))];
    idx2 = size(M,1);
    idx{l/2+1} = idx1+1:idx2;
end;

M = M/sqrt(size(n,2));

% 
% %% simulates the signal of the 3-comp model with dispersion kapps
% function S = simSig(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,ten,n,weights,disptype,kappa)
%  S = 0;
%  tr = squeeze(ten(1,1,:)+ten(2,2,:)+ten(3,3,:));
%  
%  if  strcmp(disptype,'fod'),
%      q2 = squeeze(ten(1,1,:));
%      Eintra = bd_dispstick(Dpara_in,kappa,[],ten,disptype);
%      Eextra = bd_dispstick(Dpara_ex-Dorth,kappa,[],ten,disptype) .* exp(-tr*Dorth); 
%      Ecsf = exp(-tr*3);
%      S = vf*Eintra+ (1-vf-vf_csf)*Eextra + vf_csf*Ecsf ;
%      return;
%  end;
%  
%  for k = 1:size(n,2),   
%      q2 = squeeze((n(1,k).^2 *ten(1,1,:) + n(2,k).^2 *ten(2,2,:) + n(3,k).^2 *ten(3,3,:) + 2*(n(1,k)*n(2,k)*ten(1,2,:) + n(3,k)*n(2,k)*ten(3,2,:) + n(1,k)*n(3,k)*ten(1,3,:)))); 
%      if strcmp(disptype,'nodisp') ~= 1,
%          Eintra = bd_dispstick(Dpara_in,kappa,[],[q2(:) (tr(:)-q2(:))]',disptype);
%          Eextra = bd_dispstick(Dpara_ex-Dorth,kappa,[],[q2(:) (tr(:)-q2(:))]',disptype) .* exp(-tr*Dorth); 
%      else
%         Eintra = exp(-q2*Dpara_in);
%         Eextra = exp(-q2*Dpara_ex-(tr-q2)*Dorth); 
%      end;
%      Ecsf = exp(-tr*3);
%      S = S + weights(k)*(vf*Eintra+ (1-vf-vf_csf)*Eextra + vf_csf*Ecsf );
%  end;
% 
%  
%  



%% simulates the signal of the 3-comp model with dispersion kapps
function S = simSig(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,proto,n,weights,disptype,kappa)
 S = 0;

 ten = proto.ten;
 
 tr = squeeze(ten(1,1,:)+ten(2,2,:)+ten(3,3,:));
 
 
 for k = 1:size(n,2),   
    q2 = squeeze((n(1,k).^2 *ten(1,1,:) + n(2,k).^2 *ten(2,2,:) + n(3,k).^2 *ten(3,3,:) + 2*(n(1,k)*n(2,k)*ten(1,2,:) + n(3,k)*n(2,k)*ten(3,2,:) + n(1,k)*n(3,k)*ten(1,3,:)))); 
     if strcmp(disptype,'nodisp') ~= 1,
         Eintra = bd_dispstick(Dpara_in,kappa,n,proto,disptype);
         Eextra = bd_dispstick(Dpara_ex-Dorth,kappa,n,proto,disptype) .* exp(-tr*Dorth); 
     else
         Eintra = bd_dispstick(Dpara_in,1,n,proto,'poisson');
         Eextra = bd_dispstick(Dpara_ex-Dorth,1,n,proto,'poisson') .* exp(-tr*Dorth); 
     end;
     Ecsf = exp(-tr*3);
     S = S + weights(k)*(vf*Eintra+ (1-vf-vf_csf)*Eextra + vf_csf*Ecsf );
 end;

 
 
 
 


 
 