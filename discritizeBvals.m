

function [buni b] = discritizeBvals(ten)
%%
b = squeeze(ten(1,1,:)+ten(2,2,:)+ten(3,3,:))-eps;
v = -0.1:0.1:10;
h = imfilter(histc(b,v),fspecial('gaussian',[7 1])) - 0.00001*v';
buni = v(find(h>imdilate(h,[1 0 1]'))+1);
[~,idx] = min(abs(repmat(b,[1 length(buni)]) - repmat(buni,[length(b) 1])),[],2);
b = buni(idx);

end



