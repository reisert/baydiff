function str = createDWIHashnum(ten)




for k = 1:size(ten,3),
    for j = 1:size(ten,3),
        tr(k,j) = trace(ten(:,:,k)*ten(:,:,j)')/1000;    
    end;
end;

hashnum = log(1+histc(tr(:),-50:400:10000));


str = hash2Str(hashnum);

function str = hash2Str(hashnum)           
    X = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM123456789';
    hashnum = abs(hashnum);
    str = [];
    for k = 1:length(hashnum),
        str = [str  X(1+fix(rem(hashnum(k)*length(X).^(-1:6),length(X))))];
    end;
    %str = strrep(str,'q','');
    str = str(1:min(length(str),52));
 

