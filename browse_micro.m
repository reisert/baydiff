%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes and displays microstructure diffusion parameters
%
%  browse_micro(data,wm,varargin)
%
%  data - contains the signal as a struct as defined in baydiff.m
%  wm - a mask (optional)
%  varargin - passed to baydiff
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



 

function browse_micro(data,wm,varargin)

[ds.img ds.model]  = baydiff(data,varargin{:});

if nargin < 2,
    wm = single(data.dwi(:,:,:,1)*0+1);
else
    if isempty(wm),
        wm = single(data.dwi(:,:,:,1)*0+1);   
    else
        wm = single(wm);
    end;    
end;

ds.img = ds.img.*repmat(wm,[1 1 1 size(ds.img,4)]);
ds.hr.dataAy = data.dwi;
ds.hr.user.bTensor = data.tensor;


xx = load('dwidirections');
proto.dirs256 = xx.dirs256;

proto.ten = data.tensor/1000;

[buni b] = discritizeBvals(proto.ten);
[~,proto.dirs, proto.type, proto.bstruc] = bd_getDirs(proto.ten);

proto.b = b;
proto.buni = buni;
proto.types = unique(proto.type);
proto.shprojmat = ds.model.shprojmat;

proto.cos = proto.dirs'*proto.dirs256;


ds.proto = proto;
%%
f = gcf;
clf;
set(f,'color',[1 1 1])
showfun = @(x) imshow(x,[0 1]);


ran = {[0 3.5] , [0 3.5], [0 3.5], [0 1], [0 1], [0 1]};
style = {'r--','g--','b','k--','m','c'};
mask = wm;
bb = (0:0.01:1);
subplot(3,5,13);
for k = 1:3,
    bins = bb*(ran{k}(2)-ran{k}(1)) + ran{k}(1);
    m = ds.img(:,:,:,k);
    h = histc(m(mask(:)>0 & m(:)>ran{k}(1) & m(:)<ran{k}(2)),bins);
    h = h /max(h);
    plot(bins,h,style{k},'linewidth',2); hold on;
end;
grid on;
axis([0 3 0 1]);
xlabel('diffusivities');
subplot(3,5,14);
for k = 4:6,
    bins = bb*(ran{k}(2)-ran{k}(1)) + ran{k}(1);
    m = ds.img(:,:,:,k);
    h = histc(m(mask(:)>0 & m(:)>ran{k}(1) & m(:)<ran{k}(2)),bins);
    h = h /max(h);
    plot(bins,h,style{k},'linewidth',2); hold on;
end;
grid on;
axis([0 1 0 1]);
xlabel('volume fractions');

%set(datacursormode,'UpdateFcn',{@(x,y) labeldtips(x,y,ds)})
    
%datacursormode on
%%
%f = figure('Position',[360,500,750,585]);    
c = uicontrol('Style','popupmenu','String',ds.model.parameter_names,'Position',[0,25,100,25],'Tag','contrastcombo','Value',4,'Callback',{@combo_Callback,gcbo,[],[],ds,showfun});
h = uicontrol('Style','slider','String','Surf','Position',[0,0,300,25],'Tag','theslider','Value',0.5,'Callback',{@resolutionslider_Callback,gcbo,[],[],ds,showfun});
botPanel = uibuttongroup('Units','pixels','Position',[400 0  200 30],'SelectionChangeFcn',{@radiob_Callback,gcbo,ds},'Tag','butgroup');
hX = uicontrol('Style','radiobutton','String','X','Position',[10,1,40,25],'Tag','1','Parent',botPanel);
hY = uicontrol('Style','radiobutton','String','Y','Position',[60,1,40,25],'Tag','2','Parent',botPanel);
hZ = uicontrol('Style','radiobutton','String','Z','Position',[110,1,40,25],'Tag','3','Parent',botPanel,'value',true);
coord = uicontrol('Style','text','String','x:1','Tag','coordinfo','Position',[300,0,100,25]);
set(h,'SliderStep',[1/size(ds.img,1) 0.5]);
showslice(ds.img,showfun);

colormap hot





set(f,'WindowButtonDownFcn',{@mousepressed_Callback,ds});


% --------------------------------------------------------------------
function varargout = mousepressed_Callback(h, eventdata,varargin)
    ds = varargin{1};

    if strcmp(get(gca,'tag'),'map')
        p = round(get(gca,'CurrentPoint')); p = p(1,1:2);
        hc = findobj(gcf,'Tag','haircross');
        delete(hc);
        rectangle('Position',[p-1 2 2],'Tag','haircross');
        
        p
        labeldtips(ds,p);
       
    end;


% --------------------------------------------------------------------
function varargout = combo_Callback(h, eventdata, handles, varargin)
 ds = varargin{3};
    showslice(ds.img,varargin{4});
  %  val = get(findobj(gcf,'tag','contrastcombo'),'value');
  %  varargin{3}.img = varargin{3}.maps(:,:,:,val);

% --------------------------------------------------------------------
function varargout = resolutionslider_Callback(h, eventdata, handles, varargin)
  
    ds = varargin{3};
    showslice(ds.img,varargin{4});
    
% --------------------------------------------------------------------
function varargout = radiob_Callback(h, eventdata, handles, varargin)
    img = varargin{1}.img;
     
    radiob = get(h,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    
    slider = findobj(gcf,'Tag','theslider');
    set(slider,'SliderStep',[1/size(img,selection) 0.5]);
    cb = get(slider,'Callback');
    
    showslice(img,cb{6});
    
function showslice(img,showfun);
    subplot(3,5,[1 2 6 7 11 12]);
    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos*(size(img,selection)-1))+1;
    cinfo = findobj(gcf,'Tag','coordinfo');
    
    switch selection,
        case 3,
            showfun2(squeeze(img(:,:,idxpos,:)),showfun);
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(img,3)));    
        case 2,
            showfun2(flipdim(permute(squeeze(img(:,idxpos,:,:)),[2 1 3]),1),showfun);
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(img,2)));    
        case 1,
            showfun2(flipdim(permute(squeeze(img(idxpos,:,:,:)),[2 1 3]),1),showfun); 
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(img,1)));    
    end;
    
function showfun2(img,sf)
    val = get(findobj(gcf,'tag','contrastcombo'),'value');
    if val >= 4 && val <=6,
        imagesc(img(:,:,val),[0 1]);
    elseif val == 1 || val == 7,
        imagesc(img(:,:,val),[0 3]);
    elseif val == 2 ,
        imagesc(img(:,:,val),[0.5 2]);
    elseif val == 9  || val == 8 ,
        imagesc(img(:,:,val),[0 1]);
    elseif val == 3 | val == 10,
        imagesc(img(:,:,val),[0 1]);
    elseif val == 11,
        imagesc(img(:,:,val),[0 1]);
    else
        imagesc(img(:,:,val)); %,[0 1]);
    end;    
    pos = get(gca,'position');
    set(gca,'position',[pos(1)-0.1 pos(2) pos(3)*1.3 pos(4)]);
    set(gca,'tag','map');
    colorbar;
    axis off;
     
function output_txt = labeldtips(ds,cursorpos)
    

    img = ds.img;
    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos*(size(img,selection)-1))+1;
    cinfo = findobj(gcf,'Tag','coordinfo');
    %cursorpos = get(event_obj,'Position');    
    contrast = get(findobj(gcf,'tag','contrastcombo'),'value');
    switch selection,
        case 3,
            c = [cursorpos([2 1]) idxpos];
        case 2,
            c = [cursorpos(1) idxpos size(img,3)-cursorpos(2)+1 ];
        case 1,
            c = [idxpos cursorpos(1) size(img,3)-cursorpos(2)+1];
    end;

    
    b = ds.proto.b;
    type = ds.proto.type;
    types = unique(type);
     
    Smeas = double(squeeze(ds.hr.dataAy(c(1),c(2),c(3),:)));
    Smeas = Smeas ./ mean(Smeas(b==0));

    buni = ds.proto.buni;
    dirs = ds.proto.dirs;

        
    %% get parameters

    Dpara_in =  ds.img(c(1),c(2),c(3),1);
    Dpara_ex =  ds.img(c(1),c(2),c(3),2);
    Dorth =   ds.img(c(1),c(2),c(3),3);
    vf =      ds.img(c(1),c(2),c(3),4);
    vf_csf = ds.img(c(1),c(2),c(3),5);
    vf_ex = ds.img(c(1),c(2),c(3),6);
    p2 = ds.img(c(1),c(2),c(3),11);
    snr = ds.img(c(1),c(2),c(3),end);
        
    %%  pliot current values in histogram

    subplot(3,5,13);
    delete(findobj(gca,'tag','hbars'));
    style = {'r','g','b','k','m','c'};     
    line([1 1]*Dpara_in,[0 1],'tag','hbars','color',style{1});
    line([1 1]*Dpara_ex,[0 1],'tag','hbars','color',style{2});
    line([1 1]*Dorth,[0 1],'tag','hbars','color',style{3});
    subplot(3,5,14);
    delete(findobj(gca,'tag','hbars'));
    line([1 1]*vf,[0 1],'tag','hbars','color',style{4});
    line([1 1]*vf_csf,[0 1],'tag','hbars','color',style{5});
    line([1 1]*vf_ex,[0 1],'tag','hbars','color',style{6});
  %% estimate dispersion/fod
  
    disptypes = {'poisson','fod'};
    %disptypes = {'poisson'};
    noisedeg = 1;
    res = bd_fitDispersion(Dpara_in,Dpara_ex,Dorth,vf,vf_csf,p2, snr,noisedeg, ds.proto, Smeas,ds.proto.dirs256,disptypes);
    
    
  %% plot signal/predictions

  
    subplot(3,5,[7+2]);
    title('no planar encoding');
  

    cla
    style = {'-','-.','.','--'};

    col = 'rgbckrgbckrgbmckyrgbmcky';
    colcnt = 1;
    for r = 1:length(types),
        subplot(3,5,[7+r]);
        for k = 2:length(buni),
             bidx = find(b==buni(k) & type == types(r));
             proj = (res.pd'*dirs(:,bidx)).^2;
             [proj sidx] = sort(proj);

             for j = 1:length(disptypes),
                 plot(proj,res.S{j}(bidx(sidx)),[style{j} col(colcnt)]); hold on;
             end;

            plot(proj,(squeeze(Smeas(bidx(sidx)))),['*' col(colcnt)]); hold on;

            colcnt = colcnt + 1;
        end;

        line([0 1],[0 0],'color','k');
        hold off;
        axis([0 1 0 1]);
        grid on;
        xlabel('cos(theta)^2 to principal direction')
        ylabel('normalized signal');
        if types(r) == 'l',
            title('linear encoding');
        else
            title('planar encoding');
        end

    end;
    

   %% show the fod gylph
    
   subplot(3,5,10);
   cla;
   showGlyph(ds.proto.dirs256,res.fod);
   axis equal;
   grid on;
   title('fod glyph');
   
   %% plot spherical avg (l=0) and second order (l=2)
   
   subplot(3,5,3);
   cla; axis off;
   pos = get(gca,'position');
   axes('position',pos);
   fac = -8;
   meanS = ones(length(types),1);
   meanSL2 = zeros(length(types),1);
   for r = 1:length(types),
       for k = 2:length(buni),
           bidx = find(b==buni(k) & type == types(r));        
           meanS(r,k) = mean((Smeas(bidx(:))));
           leg = legendre(2,abs(res.pd'*dirs(:,bidx)));
           meanSL2(r,k) = fac*leg(1,:)*(Smeas(bidx(:))) / length(bidx);
       end;
   end;


   plot(buni,(meanS),'*'); hold on;
   plot(buni,(meanSL2)/res.p2,'o'); hold on;



   myb0 = res.S{1}(1); 
   bval = (0.0001:0.1:max(buni));
 
    [f1l f2l f1p f2p] = avgModel(Dpara_in,Dpara_ex-Dorth,Dorth,3,vf,vf_csf,vf_ex,bval);

    plot(bval,myb0*f1l,'b');
    plot(bval,myb0*f1p,'g');
 
    plot(bval,myb0*fac*f2p,'g');
    plot(bval,myb0*fac*f2l,'b');
 

   xlabel('bval');
   ylabel('signal (SDE - blue, DDE - green)')
   grid on

   axis([0 max(buni) -1 1]);
       
   %% plot more
   
       subplot(3,5,4);
       cla; axis on;
       if size(meanSL2,1) > 1,
           
           p2pred =  7/4/fac*(meanSL2(1,:) + 2*meanSL2(2,:)) ./ (meanS(1,:)-meanS(2,:));
           plot(buni(2:end), p2pred(2:end),'m*');hold on;
       end;
       
        p2pred =  res.p2*7/4*(f2l + 2*f2p) ./ (f1l-f1p); 
        plot(bval(15:end), p2pred(15:end),'m-');
        axis([0 max(buni) 0 1]);
       
       grid on;
       
   %% print acual parameters

   subplot(3,5,5);
    cla;
    str = [];
   % str = sprintf('imval: %i \n',ds.img(c(1),c(2),c(3),contrast));
    str = [str sprintf('v intra : %.3f \n',vf(1))];
    str = [str sprintf('v extra : %.3f  \n',vf_ex(1))];
    str = [str sprintf('v csf : %.3f  \n',vf_csf(1))];
    str = [str 'Dax intra:  ' sprintf('%.2f ',Dpara_in(1)) sprintf('\n')];
    str = [str 'Dax extra:  ' sprintf('%.2f ',Dpara_ex(1)) sprintf('\n')];
    str = [str 'Drad extra: ' sprintf('%.2f ',Dorth(1)) sprintf('\n')];
    str = [str 'cond: ' sprintf('%.2f ',   Dpara_in(1)*Dorth(1) - Dpara_in(1)*3 + (Dpara_ex(1)-Dorth(1))*3 ) sprintf('\n')];
    str = [str 'p2:  ' sprintf('%.2f ',res.p2) sprintf('\n')];
    str = [str 'snr:  ' sprintf('%.2f ',snr(1)) sprintf('\n')];
   % str = [str sprintf('pos %i %i %i ',c(1),c(2),c(3)) sprintf('\n')];
    for k = 1:length(disptypes),
        str = [str disptypes{k} ' loglik (chi/gau):  ' sprintf('%.2f/%.2f ',res.errRic(k),res.errGS(k)) sprintf('\n')];
    end;
    text(0,0,str); %,'fontsize',10,'fontunits','normalized');
    axis([0 50 -50 50]);
    axis off;

    output_txt = sprintf('%.5f',ds.img(c(1),c(2),c(3),contrast));
        
        
        
        
        
        
        
        
        
        
        

function C = cmult(A,B,idxA,idxB)

szA = size(A);
szB = size(B);

szA = [szA ones(1,max(idxA)-length(szA))];
szB = [szB ones(1,max(idxB)-length(szB))];

if nargin == 2,
    idxA = length(szA);
    idxB = 1;
end;

permA = 1:length(szA);
remainA = setdiff(permA,idxA);
permA = [remainA idxA];

permB = 1:length(szB);
remainB = setdiff(permB,idxB);
permB = [idxB setdiff(permB,idxB)];


C = reshape( ...
    reshape(permute(A,permA),[prod(szA(remainA)) prod(szA(idxA))]) * ...
    reshape(permute(B,permB),[prod(szB(idxB)) prod(szB(remainB))]) ...
    ,[szA(remainA) szB(remainB) 1]);



        
function [f1l f2l f1p f2p] = avgModel(D1,D2,D3,Df,v,w,u,b) 

f1l = real(w.*exp(-Df.*b) + (pi.^(1./2).*v.*myERF(D1.^(1./2).*b.^(1./2)))./(2.*D1.^(1./2).*b.^(1./2)) + (pi.^(1./2).*u.*myERF(D2.^(1./2).*b.^(1./2)).*exp(-D3.*b))./(2.*D2.^(1./2).*b.^(1./2)));
f2l = -real((3.*v.*(exp(-D1.*b)./(2.*D1.*b) + (pi.^(1./2).*myERFi((-D1.*b).^(1./2)))./(4.*D1.^(3./2).*(-b).^(3./2))))./2 + (3.*u.*((exp(-D2.*b).*exp(-D3.*b))./(2.*D2.*b) + (pi.^(1./2).*exp(-D3.*b).*myERFi((-D2.*b).^(1./2)))./(4.*D2.^(3./2).*(-b).^(3./2))))./2 + (pi.^(1./2).*v.*myERF(D1.^(1./2).*b.^(1./2)))./(4.*D1.^(1./2).*b.^(1./2)) + (pi.^(1./2).*u.*myERF(D2.^(1./2).*b.^(1./2)).*exp(-D3.*b))./(4.*D2.^(1./2).*b.^(1./2)));
f1p = real(w.*exp(-Df.*b) + (2.^(1./2).*pi.^(1./2).*v.*exp(-(D1.*b)./2).*myERF((2.^(1./2).*D1.^(1./2).*(-b).^(1./2))./2))./(2.*D1.^(1./2).*(-b).^(1./2)) + (2.^(1./2).*pi.^(1./2).*u.*exp(-D3.*b).*exp(-(D2.*b)./2).*myERF((2.^(1./2).*D2.^(1./2).*(-b).^(1./2))./2))./(2.*D2.^(1./2).*(-b).^(1./2)));
f2p = real((3.*u.*(exp(-D3.*b)./(D2.*b) - (2.^(1./2).*pi.^(1./2).*exp(- (D2.*b)./2 - D3.*b).*myERFi((2.^(1./2).*D2.^(1./2).*b.^(1./2))./2))./(2.*D2.^(3./2).*b.^(3./2))))./2 + (3.*v.*(1./(D1.*b) - (2.^(1./2).*pi.^(1./2).*exp(-(D1.*b)./2).*myERFi((2.^(1./2).*D1.^(1./2).*b.^(1./2))./2))./(2.*D1.^(3./2).*b.^(3./2))))./2 - (2.^(1./2).*pi.^(1./2).*u.*exp(- (D2.*b)./2 - D3.*b).*myERF((2.^(1./2).*D2.^(1./2).*(-b).^(1./2))./2))./(4.*D2.^(1./2).*(-b).^(1./2)) - (2.^(1./2).*pi.^(1./2).*v.*exp(-(D1.*b)./2).*myERF((2.^(1./2).*D1.^(1./2).*(-b).^(1./2))./2))./(4.*D1.^(1./2).*(-b).^(1./2)));


function x = myERF(y)

if abs(imag(y(1))) > 0,
    x = i*erfi_(imag(y));
else
    x = erf(y);
end;

function x = myERFi(y)

if abs(imag(y(1))) > 0,
    x = -i*erf(-imag(y));
else 
    x = erfi_(y);
end;


function ans=erfi_(x)


xc=5.7;%cut for asymptotic approximation (when x is real)
ans=~isreal(x).*(-(sqrt(-x.^2)./(x+isreal(x))).*gammainc(-x.^2,1/2))+...
    isreal(x).*real(-sqrt(-1).*sign(x).*((x<xc).*gammainc(-x.^2,1/2))+...
    (x>=xc).*exp(x.^2)./x/sqrt(pi));



