

function [scheme scheme_norm weighting bstruc] = bd_getDirs(ten)
    
for k = 1:size(ten,3),
    [U D] = eigs(ten(:,:,k));
    d = diag(D);
    diffs = [abs(d(3)-d(2)) abs(d(1)-d(3)) abs(d(1)-d(2))];
    [~,idx] = min(diffs);    
    scheme(:,k) = real(sqrt((D(idx,idx))+0.001)*U(:,idx));
    scheme_norm(:,k) = U(:,idx);

    
    
    Biso = min(d);
    Bten = max(d)-min(d);
    if abs(Biso-d(2)) < 0.01,
        weighting(k) = 'l';
    else        
        weighting(k) = 'p';
    end;        

    bstruc(1,k) = Biso;
    bstruc(2,k) = Bten;
    
end
            

% 
% for k = 1:size(ten,3),
%         [U D] = eigs(ten(:,:,k));
%         [~,idx] = max(diag(D));
%         scheme(:,k) = sqrt(D(idx,idx))*U(:,idx);
%         scheme_norm(:,k) = U(:,idx);
%     end;
% end
%             
% 
