

function showGlyph(bDir,data1)
    K = convhulln(double(bDir'));
    %data = data / max(data(:));    
    data = data1;
    data(data<0) = 0;
    d = bDir.* (ones(3,1)*(data(:))');    
    patch('Faces',K,'Vertices',d','facecolor','interp','edgecolor','none','FaceVertexCData',data(:),'CDataMapping','scaled','facealpha',0.9);

    data = -data1;
    data(data<0) = 0;
    d = bDir.* (ones(3,1)*(data(:))');    
    patch('Faces',K,'Vertices',d','facecolor','red','edgecolor','none','CDataMapping','scaled','facealpha',0.1);
