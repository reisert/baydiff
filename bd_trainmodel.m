function modelstruc = trainmodel(data,varargin)



    % simulation parameters
    N = 1;                 % number of coils (noisetype)                                 (badifparam)
    nt = 0.5*10^4;           % number of training samples                                  (badifparam)
    ncross = [1 2 30];     % number of crossing fibers                                   (badifparam)
    traceequality = true;    
    tracetol = 1.5;
    
    
    % model parameters
    lmax = 2;              % SH expansion degree                                         (badifparam)
    reg = 10^-8;           % Tikhonov regularizer for Bayes model                        (badifparam)
    includeb0 = true;      % sensitivity to b0                                           (badifparam)
    bayesmodel = 'poly';   % bayes model type (fourier or poly)                          (badifparam)
    order = 3;             % complexity of the model                                     (badifparam)
    qspace = 'multishell';      % radial sampling ('multishell' or 'qball')                   (badifparam)
    nmax = 2; D0 = 1;      % qball parameters, r_k(b) = b^k exp(-D0 b) with 0<=k<=nmax   (badifparam)
        
    p2estimation = false;
    
    % general 
    verbose = true;        % show correlations during learing                            (badifparam)
    force_retraining = 1;  % train even if saved model is found                          (badifparam)
    noise = 'estlocal';    % type of noise estimation ('estlocal','estglobal',SNRmap,nz) (badifparam)
    SNRthreshold = 5;

    
    
for k = 1:2:length(varargin),
    if exist(varargin{k}),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;



ten = data.tensor/1000;
[buni b] = discritizeBvals(ten);
[scheme, ~, type] = bd_getDirs(ten);


if min(buni) ~= 0,
    error('no b0 available ...');
end;


types = unique(type);
display('DWI scheme');
for k = 1:length(buni),
    for j = 1:length(types),
        if buni(k) == 0,
            if j == 1,
                fprintf('bval 0, num %i\n',sum(b==0));
            end;
        else
            fprintf('bval %i, type: %c    num %i\n',round(buni(k)*1000),types(j), sum(buni(k)==b & types(j)==type));
        end
    end
end;        
drawnow;


display('drawing parameters');
% sweep through different SNRs
nz = 0.01;      % 1/nz is maximal SNR simulated
B0 = (0.3:0.01:1).^4; % SNR = B/nz

% %%%%%%% prior distributions

D1 = 0.2+rand(nt,1)*2.8;   % intra axonal diffusion
D2 = 0.2+rand(nt,1)*2.8;   % extra axonal diffusion parallel
D3 = 0.2+rand(nt,1)*2.8;   % extra axonal diffusion perpendicular



V = rand(nt,1);            % intra axonal fraction
Vw = rand(nt,1);           % fast water fraction
Vex = rand(nt,1);

Vblood = 0.*rand(nt,1);    % blood fraction, experimental
                           % you can remove artefacts by this coming
                           % from phase cancelations in b0

Vsum = Vw + Vblood + Vex;
Vw =     (1-V).* Vw./Vsum;
Vex =    (1-V).* Vex./Vsum;
Vblood = (1-V).* Vblood./Vsum;

if traceequality
    idx = find(abs(D1-(D2+2*D3))<tracetol & D2>D3);  % select those with similar traces
else
    idx = find(D2>D3);  
end
D1 = D1(idx);
D2 = D2(idx);
D3 = D3(idx);
V = V(idx);
Vw = Vw(idx);
Vex = Vex(idx);
Vblood = Vblood(idx);

%%%%%%% define derived parameters
meanD = ((D1(:).*V(:) + (D2(:)+2*D3(:)).*(1-(V(:)+Vw(:))) + 9*Vw(:))/3) ;
microAx = (D1(:).*V(:) + (D2(:)).*(1-(V(:)+Vw(:))) + 3*Vw(:));
%microRad = ( (D3(:)).*(1-(V(:)+Vw(:))) + 3*Vw(:));
%microRad = double(D1(:)>D2(:));
microAx = double(D1(:)>D2(:));
microRad = double(D1(:)./(D1(:)+D2(:)));
microFA = sqrt(3/2*( (microAx-(microAx+2*microRad)/3).^2 + 2*(microRad-(microAx+2*microRad)/3).^2) ./ (microAx.^2 + 2* microRad.^2));

%stroke = double( (D1(:) < 1.5 & V(:) > 0.75 & D3(:) < 0.7) | (D3(:) < 0.3 & Vw(:) < 0.2) );
stroke = double( (D1(:) < 1.3 & V(:) > 0.9  & D3(:) < 0.5) );

% number of samples
numsamples = length(D1(:));



%%





%% simulate Signal
display('simulating signal');

genSig = @(D1,D2,D3,V,Vw,pb) repmat(V(:),[1 size(pb,2)]).*exp(-repmat(D1(:),[1 size(pb,2)]).*pb) + ...
        repmat(Vex(:),[1 size(pb,2)]).*exp(-repmat(D2(:),[1 size(pb,2)]).*pb-repmat(D3(:),[1 size(pb,2)]).*(repmat(b,[size(pb,1) 1])-pb) ) +...
        repmat(Vw(:),[1 size(pb,2)]).*exp(-repmat(D1(:)*0+3,[1 size(pb,2)]).*repmat(b,[size(pb,1) 1])) + ...
        repmat(-Vblood(:),[1 size(pb,2)]).*exp(-repmat(D1(:)*0+20,[1 size(pb,2)]).*repmat(b,[size(pb,1) 1])) ;

Signal = [];    
p2 = [];
for k = 1:length(ncross),    
    Signal_tmp = 0;
    W = 0;
    
    SH2 = 0;
    for j = 1:ncross(k),    

        n = randn(3,numsamples); 
        n = n./repmat(sqrt(sum(n.^2)),[3 1]);
        pb = n(1,:)'.^2 * squeeze(ten(1,1,:))' + ...
             n(2,:)'.^2 * squeeze(ten(2,2,:))' + ...
             n(3,:)'.^2 * squeeze(ten(3,3,:))' + ...
             2* (n(1,:).*n(2,:))' * squeeze(ten(1,2,:))' + ...
             2* (n(1,:).*n(3,:))' * squeeze(ten(1,3,:))' + ...
             2* (n(2,:).*n(3,:))' * squeeze(ten(2,3,:))' ;
    
        Stmp = genSig(D1,D2,D3,V,Vw,pb);
        Wtmp = rand(numsamples,1);
        Signal_tmp = Signal_tmp + repmat(Wtmp,1,size(Stmp,2)).*Stmp;
                
        SH2 = SH2 +repmat(Wtmp',[6 1]).*([n(1,:).^2 ; n(2,:).^2 ; n(3,:).^2 ; sqrt(2)*n(1,:).*n(2,:) ; sqrt(2)*n(1,:).*n(3,:) ; sqrt(2)*n(2,:).*n(3,:)]);

        
        W = W + Wtmp;
        
    end;
    
    SH2 = SH2 ./ repmat(W',[6 1]);
    SH2(1:3,:) = SH2(1:3,:) - repmat(mean(SH2(1:3,:)),[3 1]);
    p2 = cat(2,p2,sum(SH2.^2)*3/2);
    
    Signal_tmp = Signal_tmp./repmat(W,1,size(Signal_tmp,2));
    Signal = [Signal ; Signal_tmp];
end;






%% train Bayes model

display('training');
beta = [];

for j = 1:length(B0),
    
    % simulate noise and normalization by b0
    S0 = B0(j)*Signal;    
    S = 0;
    for k = 1:N,
       S = S + abs(S0 + nz*sqrt(N)*(randn(size(S0))+1i*randn(size(S0)))).^2;
    end;
    S = sqrt(S/N);
    S = S./ repmat(mean(S(:,b==0),2),[1 size(S,2)]);
   
    
    
    %%%%%%%%%%%%%%%%%%% training    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [M shprojmat] = compPowerSpec(b,scheme,type,lmax,S,@(x) x,qspace,nmax,D0)    ;
    A = compMesoInv(M,lmax,order,includeb0,bayesmodel);
    P = compRegress(A,lmax,order,includeb0,bayesmodel);    
        
    if p2estimation
        P = cat(2,P,squeeze(sum(M(:,2,:),3)./sum(M(:,1,:),3)));
    end
    
    
    vf_linsub = 1/3;
    R = eye(size(P,2));


    target = repmat([D1(:) D2(:) D3(:) V(:)-vf_linsub Vw(:)-vf_linsub Vex(:)+Vblood(:)-vf_linsub meanD microAx microRad microFA ],[length(ncross) 1]); 
    
    target = cat(2,target,p2');
    
    stmp = repmat([stroke],[length(ncross) 1]); 
    stroke_tmp = ((sum(((A-repmat(mean(A(stmp>0,:)),[size(A,1) 1])) ./ repmat(std(A(stmp>0,:)),[size(A,1) 1])).^2,2)))<6;
    
    % stroke = double(A(:,1) > 0.04 & A(:,2) > 0.02);    
    target = cat(2,target,stroke_tmp);

    if not(exist('target_max')),
        target_max = max(target);
        target_min = min(target);
    end;
    target_max = max(target_max,max(target));
    target_min = min(target_min,min(target));
    
    
    
    
    alpha = (P'*P +reg*R*size(P,2))\(P'*target);
    pred = P*alpha;
    pred(:,[4:6]) = pred(:,[4:6]) + vf_linsub;
    target(:,[4:6]) = target(:,[4:6])  + vf_linsub;
    % some visualization
    if verbose,
        ran1 = [0.3 0.3 0.3 0 0 0.3 0 ];
        ran2 = [3 3 1.5 1 1 1 1];
      
        cnt = 0; 
        for a = [1 2 3 4 5 6 11],
            cnt = cnt + 1;
            subplot(1,7,cnt);
            delta = 0.03;
            bi = ((0:delta:1-delta/2) + delta/2)*(ran2(cnt)-ran1(cnt)) + ran1(cnt);
            idx = pred(:,cnt)>ran1(cnt) & pred(:,cnt)<ran2(cnt);
            h = hist3([pred(idx,a),target(idx,a)],{bi bi});
            h = h ./ repmat(sum(h),[size(h,1) 1]);
            imagesc(bi,bi,h);
            axis xy
            xlabel('ground truth');
            ylabel('prediction');
            colormap hot
            if a == 1, title(sprintf('SNR %.1f ',B0(j)/nz)); end;
        end
        
        drawnow;
    end;
    
    ALPHA{j} = alpha;
    err = sqrt(  sum((pred-target).^2)./ sum((target - repmat(mean(target),[size(target,1) 1])).^2) );

end;


%% apply the model
function est = apply(SNR,data,ten,spatpreprocfun)

    

[buni b] = discritizeBvals(ten/1000);
[scheme scheme_norm type] = bd_getDirs(ten/1000);


S0 = SNR*nz;

M = compPowerSpec(b,scheme,type,lmax,data,spatpreprocfun,qspace,nmax,D0)    ;
A = compMesoInv(M,lmax,order,includeb0,bayesmodel);
P = compRegress(A,lmax,order,includeb0,bayesmodel);  

if p2estimation
    P = cat(2,P,squeeze(sum(M(:,2,:),3)./sum(M(:,1,:),3)));
end

 
P(isnan(P(:)))=0;

B0edge = [-inf (B0(2:end)+B0(1:end-1))*0.5 inf];
est = zeros(size(P,1),size(ALPHA{1},2));
for k = 1:length(B0),
    idx =  ((S0>B0edge(k) & S0<=B0edge(k+1)));
    Q = P(idx,:);
    est(idx,:) = Q*ALPHA{k};    
end;

for k = 1:length(target_max),
    est(est(:,k)>target_max(k),k) = target_max(k);
    est(est(:,k)<target_min(k),k) = target_min(k);
    
end;

est(:,[4:6]) = est(:,[4:6]) + vf_linsub;


est(:,11) = est(:,11).*(pi/2+atan(-(est(:,7)-1)*10))/pi; % soft thresholding of stroke prob with meanD


est(SNR<SNRthreshold,:) = nan;


% %% if you want color-coded v_intra
% b = sum(scheme.^2);
% s = reshape(ten(:,:,:),[3*3 length(b)])'; s = pinv(s'*s);
% p = log(1+abs(data(:,:)))*reshape(ten,[3*3 length(b)])'*s;
% p = permute(reshape(p,[size(data,1) 3 3]),[2 3 1]);
% p = fasteigs(p);
% v = squeeze(p(:,1,:));
% v = repmat(est(:,4),[1 3]).*v';

%%
est = real(est);
est = cat(2,est,SNR);


end


%% save all the stuff

meanPrior = mean(target);

clear pb data S S0 S1 Stmp target pred n V Vw W Wtmp

modelstruc.lmax = lmax;
modelstruc.order = order;
modelstruc.maxnz = nz;
modelstruc.includeb0 = includeb0;
modelstruc.bayesmodel = bayesmodel;
modelstruc.prepFeats = @prepFeats;
modelstruc.shprojmat = shprojmat;
modelstruc.apply = @(x,y,ten,pp) apply(x,y,ten,pp);
modelstruc.feats = @(y) feats(y,lmax,scheme,b);
modelstruc.noisedeg = N;
modelstruc.meanPrior = meanPrior;
modelstruc.parameter_names = {'Dax_intra','Dax_extra','Drad_extra','v_intra','v_csf','v_extra','microADC','microAx','microRd','microFA','p2','SNR'};



end




%% compute rotations invariant SH features
function [M projMat] = compPowerSpec(b,scheme,type,lmax,S,pp,qspace,nmax,D0)

    M = [];
         
    buni = unique(b);
    types = unique(type);
    
    S(S(:)>2) = 2; % is that needed?
       
    %%%%%% projections
    if strcmp(qspace,'qball');
        dirs = scheme(:,:);
        [proj_tmp idx_sh] = SH(dirs+eps,lmax); proj_tmp = pinv(proj_tmp);
        bvals = repmat(b(:),[1 size(proj_tmp,2)]);
        for l = 1:length(idx_sh),
            for k = 1:nmax, 
                tmp = (proj_tmp.*bvals.^(k-1).*exp(-D0*bvals)/factorial(k-1))/sqrt(length(b));
                projMat{k,l} = tmp(:,idx_sh{l});
                proj{k,l} = S*projMat{k,l};
            end;
        end;    
    elseif strcmp(qspace,'multishell');
        dirs = scheme(:,:);
        [proj_tmp idx_sh] = SH(dirs+eps,lmax); proj_tmp = proj_tmp';        
        for l = 1:length(idx_sh),
            cnt = 1;
            for j = 1:length(types),
                for k = 2:length(buni),
                    bval = buni(k);
                    idx = not(b==bval & type==types(j));                    
                    tmp = proj_tmp;
                    tmp(idx,:) = 0;
                    projMat{cnt,l} = tmp(:,idx_sh{l}) / sqrt(sum(1-idx));
                    proj{cnt,l} = S*projMat{cnt,l};
                    cnt = cnt + 1;
                end;
            end;
        end;
    else
        error('unknown regression model');
    end;
    
    %%%%% rotation invariant features
    for k = 1:size(proj,1),
        for l = 1:size(proj,2),
            M(:,l,k) = pp((sum(abs(proj{k,l}(:,:)).^2,2)));
        end;
    end;
    
end


%% compute mesostructure invariant features
function P = compMesoInv(M,lmax,order,includeb0,bayesmodel)

     if includeb0, % for l=0 no ratios are computed
        tmp = squeeze(M(:,2:(lmax/2+1),1:end-1)./repmat( sum(abs(M(:,2:(lmax/2+1),:)),3),[1 1 size(M,3)-1]));   
        tmp = permute(tmp,[1 3 2]);         
        M = [squeeze(M(:,1,:)) tmp(:,:) ];                  
     else  % only ratios   
         M = squeeze(M(:,1:(lmax/2+1),1:end-1)./repmat(sum(abs(M(:,1:(lmax/2+1),:)),3),[1 1 size(M,3)-1]));
         M = M(:,:);
     end;
     P = M;
end

%% compute regression coefficients
function [P combidx] = compRegress(M,lmax,order,includeb0,bayesmodel)
  
    M = M;
    size(M);
    maxN = size(M,2);
    P = [M(:,1)*0+1 ];
    
    %%% fourier model
    if strcmp(bayesmodel,'fourier'),
        K = 2*pi*5/order;
        for k = 1:maxN,
            Q = permute(exp(1i*M(:,k)*[0:order-1]*K),[1 3 2]);
            P = repmat(Q,[1 size(P,2) 1]).*repmat(P,[1 1 size(Q,3)]);
            P = P(:,:);
        end;
        P = [real(P) imag(P)];
    %%% polynomial model
    elseif strcmp(bayesmodel,'poly'),
        idx = 1:maxN;
        combidx{1} = idx;
        for k = 1:order,
            tmp = prod(reshape(M(:,idx),size(M,1),size(idx,1),size(idx,2)),2);
            tmp = reshape(tmp,[size(tmp,1) size(tmp,3)]);
            P = cat(2,P,tmp);
            tmp = repmat(1:maxN,[size(idx,2) 1]);
            idx = unique(sort(cat(1,repmat(idx,[1 maxN]),tmp(:)'))','rows')';      
            combidx{1+k} = idx;            
        end,
        combidx = combidx(1:end-1);
    else
        error('model not known');
    end
end


%% Spherical Harmonic Matrix
function [M idx] = SH(n,lmax)
n = n ./ repmat(sqrt(sum(n.^2)),[3 1]);

M = [];
for l = 0:2:lmax,
    m = legendre(l,n(3,:),'sch');
    n2 = n(1,:)+i*n(2,:)+eps;
    n2 = n2 ./ abs(n2);
    m = flipdim(m,1).* (repmat(n2,[size(m,1) 1]).^repmat((l:-1:0)',[1 size(n2,2)]));
    idx1 = size(M,1);
    M = [M ; m(1:end-1,:)*sqrt(2) ; m(end,:) ];
    idx2 = size(M,1);
    idx{l/2+1} = idx1+1:idx2;
end;

M = M/sqrt(size(n,2));

end







