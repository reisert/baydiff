%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes signal of a dispersed stick
%
% S = dispstick(D, a, n,sch,type)
%    D - of the stick
%    a - conc. parameter
%    n - direction of stick
%    sch - diff scheme  (in q-space coords)
%    type -  gaussian - backprojected Gaussian
%            poisson  - Poisson kernel
%            heat     - spherical heat kernel
%            watson   - watson distibutuin
%            mises    - von Mises Fisher dist
%            fod      - dispersed by fod given sh rep
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

           
function S = dispstick(D, a, n,proto,type)

ten = proto.ten;

if strcmp(type,'gaussian')==1,
    avg = 500;
    a = -log(a)/2;
    nz = repmat(n,[1 avg]) + a * randn(3,avg);
    nz = nz ./ repmat(sqrt(sum(nz.^2)),[3 1]);
    qxx = (scheme'*nz).^2;    
    S = mean(exp(-D*qxx),2);
elseif strcmp(type,'mises')==1,
    avg = 500;
        
    kappa = 0.001;
    alpha = [ 0.0011   -0.0290    0.2473   -1.2354    3.3866   -5.0763    3.8899   -1.1908]*10^3;
    SH2ic = @(f) 1./(exp((repmat(f,[8 1]).^repmat((0:(8-1))',[1 size(f,2)]) )'*alpha')-kappa);    
    cp = SH2ic(a);
    
    nz = mf_sample(repmat(n,[1 avg])*cp);
    qxx = (scheme'*nz).^2;
    S = mean(exp(-D*qxx),2);
elseif strcmp(type,'poisson')==1,
    a = sqrt(a);
    cosp = abs(n'*(proto.dirs));    
    S = poissonDisp(ten,cosp,D,a, proto);  
elseif strcmp(type,'heat')==1,
    a = -log(a)/6;
    cosp = abs(n'*(proto.dirs));

    S = heatDisp(ten,cosp,D,a, proto);  
elseif strcmp(type,'watson')==1,
    cosp = abs(n'*(proto.dirs));
    S = watsonDisp(ten,cosp,D,a, proto);  
elseif strcmp(type,'fod')==1,   
    S = fodDisp(ten,D,a,proto); 
    
end;



function S = poissonDisp(ten,cosp,D,lam,proto)
lmax = 6;
L = (0:lmax);
f = lam.^L ;
S = SHDisp(ten,cosp,D,f,proto);


function S = heatDisp(qxx,cosp,D,lam,proto)
lmax = 6;
L = (0:lmax);
f = exp(-lam*L.*(L+1));
S = SHDisp(qxx,cosp,D,f,proto);

function S = watsonDisp(ten,cosp,D,lam,proto)

if evalin('base','exist(''watsonlut'')'),
    watsonlut = evalin('base','watsonlut');
else
    watsonlut = load('watsonlut.mat');
    assignin('base','watsonlut',watsonlut);
end;

k = 0.001;
alpha = [0.0026   -0.0449    0.3326   -1.4291    3.4388   -4.5957    3.1820   -0.8907]*10^3;
SH2c = @(f) 1./(exp((repmat(f,[8 1]).^repmat((0:(7))',[1 size(f,2)]) )'*alpha')-k);
kappa = SH2c(lam);
kappa(kappa<0) = 0;
kappa = real(kappa);
lmax = 14;
%%
f(1) = 1;
for l = 2:2:lmax,
    f(l+1) = interp1(watsonlut.k,watsonlut.lut(l/2,:),kappa,'linear','extrap') ;
end;
S = SHDisp(ten,cosp,D,f,proto);




function S = fodDisp(ten,D,f,proto)

weighting = proto.type;
types = proto.types;
buni = proto.buni;
b = proto.b;


S = zeros(length(proto.b),1);
S(b==0) = sum(f);
for s = 1:length(types),
    for k = 2:length(buni),
        idx = find(b(:)==buni(k) & types(s) == weighting(:));
        bD = D*buni(k);
        Biso = proto.bstruc(1,idx(1));
        Bten = proto.bstruc(2,idx(1));
        bD = D*Bten;        
        if types(s) == 'l',
            S(idx) = sum(exp(-bD*proto.cos(idx,:).^2).* repmat(f',[length(idx) 1 ]),2)*exp(-Biso*D);
        else
            S(idx) = sum(exp(-bD*(1-proto.cos(idx,:).^2)).* repmat(f',[length(idx) 1 ]),2)*exp(-Biso*D);
        end
    end;
end;

return;
%%


function S = SHDisp(ten,cosp,D,f,proto)

buni = proto.buni;
types = proto.types;
weighting = proto.type;
b = proto.b;

t = -1:0.001:1;
lmax = length(f)-1;
L = (0:lmax);
p = myleg(lmax,t') .* repmat(sqrt((2*L+1)),[size(t,2) 1]) /sqrt(length(t));
pinvp = pinv(p);
S = ones(length(b),1);
for j = 1:length(types),
    for k = 2:length(buni),
        idx = find(b(:)==buni(k) & types(j) == weighting(:));

        Biso = proto.bstruc(1,idx(1));
        Bten = proto.bstruc(2,idx(1));
        bD = D*Bten;
        if types(j) == 'l',
            P = ((exp(-bD*t.^2)*p).*f)*pinvp*exp(-Biso*D);
        else        
            P = ((exp(-bD*(1-t.^2))*p).*f)*pinvp*exp(-Biso*D);
        end;

        it = cosp(idx); it(it>1) = 1; it(it<0) = 0;
        S(idx) = interp1(t,real(P),real(it));    
    end;
end;



function p = myleg(n,x);
if n == 0,
    p = x*0+1;
    return;
end
if n == 1
    p = [x*0+1 x];
    return;
end;
p = zeros(size(x,1),n+1);
p(:,1:2) = [x*0+1 x];
for k = 2:n,
    p(:,k+1) = ((2*k-1)*x.*p(:,k) - (k-1)*p(:,k-1))/k;    
end;


    
function [M idx] = SH(n,lmax)
n = n ./ repmat(sqrt(sum(n.^2)),[3 1]);

M = [];
for l = 0:2:lmax,
    m = legendre(l,n(3,:),'sch');
    n2 = n(1,:)+i*n(2,:)+eps;
    n2 = n2 ./ abs(n2);
    m = flipdim(m,1).* (repmat(n2,[size(m,1) 1]).^repmat((l:-1:0)',[1 size(n2,2)]));
    idx1 = size(M,1);
    M = [M ; m(1:end-1,:)*sqrt(2) ; m(end,:) ];
 %   M = [M ; m(1,:) ; m(2:end,:) ];
%    M = [M ; m(1,:) ; real(m(2:end,:)) ; imag(m(2:end,:))];
    idx2 = size(M,1);
    idx{l/2+1} = idx1+1:idx2;
end;

%M = M/sqrt(size(n,2));



